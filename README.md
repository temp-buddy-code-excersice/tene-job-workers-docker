# Docker local architecture

<img src="https://img.shields.io/badge/docker-18.03.0-green.svg">
&nbsp;
<img src="https://img.shields.io/badge/docker--compose-3.0-green.svg">
&nbsp;


Local environtment to deploy backend, frontend, database and phppgadmin (database web administrator) interconnected between them. This project is independent of back and front, it takes files from repository (develop branch) and build them automatically, the only needed is Docker.

&nbsp;
## Usage

Clone the repository into local directory using the Docker console and run the docker-compose:
```
$ git clone https://gitlab.com/temp-buddy-code-excersice/tene-job-workers-docker
$ cd tene-job-workers-docker
$ docker-compose up -d
```

The **docker-compose.yml** file have the configuration to run and deploy all services, and the mapping ports to access them.
&nbsp;

You can use the next command to get a summary of all containers, states and ports.
```
$ docker-compose ps
```

And you get this result:

| Name                   | Command                        | State | Ports                         |
| ---------------------- | ------------------------------ | ----- | ----------------------------- |
| phppgadmin             | /sbin/entrypoint app:start     | Up    | 443/tcp, 0.0.0.0:8080->80/tcp |
| tene-job-workers-api   | java -jar target/tene-job- ... | Up    | 0.0.0.0:2030->2030/tcp        |
| tene-job-workers-db    | /docker-entrypoint.sh postgres | Up    | 5432/tcp                      |
| tene-job-workers-front | nginx -g daemon off;           | Up    | 0.0.0.0:80->80/tcp            |

( The **docker-compose up** command may take a few minutes to be ready. )

&nbsp;
## URLs


This are all URLs from the services you have access.

| Service      | Url                        |
| ------------ | -------------------------- |
| API Backend  | http://192.168.99.100:2030 |
| Web FrontEnd | http://192.168.99.100:80   |
| PHPpgadmin   | http://192.168.99.100:8080 |

&nbsp;
## Documentation

All endpoints are documentated with interactive examples in the web, in the next url: http://192.168.99.100:2030/swagger-ui.html.

You can build json request and execute ednpoints to observe the response from the API, there you have all endpoints and the models definitions.